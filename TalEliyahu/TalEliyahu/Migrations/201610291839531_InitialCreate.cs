namespace TalEliyahu.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Student",
                c => new
                    {
                        StudentIndex = c.Int(nullable: false, identity: true),
                        StudentName = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        StudentPassword = c.String(),
                        StudentEmail = c.String(),
                    })
                .PrimaryKey(t => t.StudentIndex);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserIndex = c.Int(nullable: false, identity: true),
                        UserName = c.Int(nullable: false),
                        UsertEmail = c.String(),
                    })
                .PrimaryKey(t => t.UserIndex);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
            DropTable("dbo.Student");
        }
    }
}
