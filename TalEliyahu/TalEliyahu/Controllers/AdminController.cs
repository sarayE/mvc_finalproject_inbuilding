﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace TalEliyahu
{
    public class AdminController : Controller
    {
        private TalDbContext db = new TalDbContext();

        public ActionResult Admin()
        {
            using (db = new TalDbContext())
            {
                db.Database.Initialize(true);

                ViewBag.AllStudents = db.Students.Count();
                List<Student> lstStudents = db.Students.ToList();

                ViewBag.AllUsers = db.Users.Count();
                List<User> lstUsers = db.Users.ToList();

                List<object> myModel = new List<object>();
                myModel.Add(lstStudents);
                myModel.Add(lstUsers);

                return View(myModel);
            }
        }


        public ActionResult _Delete(int? id)
        {
            using (db = new TalDbContext())
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                User user = db.Users.Find(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                return View(user);
            }
        }

        [HttpPost, ActionName("_Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            using (db = new TalDbContext())
            {
                User user = db.Users.Find(id);
                db.Users.Remove(user);
                db.SaveChanges();
                return RedirectToAction("Admin");
            }
        }
    }
}