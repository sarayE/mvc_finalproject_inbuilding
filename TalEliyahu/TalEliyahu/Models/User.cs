﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TalEliyahu
{
    public class User
    {
        [Key]
        public int UserIndex { get; set; }
        public string UserName { get; set; }
        public string UsertEmail { get; set; }
    }
}