﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TalEliyahu
{
    [Table("Student")]
    public class Student
    {
        [Key]
        public int StudentIndex { get; set; }
        public string StudentName { get; set; }
        public int StudentId { get; set; }
        public string StudentPassword { get; set; }
        public string StudentEmail { get; set; }   
    }



}