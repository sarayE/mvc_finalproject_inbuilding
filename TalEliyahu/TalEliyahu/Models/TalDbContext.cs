﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TalEliyahu
{
    public class TalDbContext : DbContext
    {
        public TalDbContext()
            : base("TalDbContext")
        {

        }

        public DbSet<Student> Students { get; set; }
        public DbSet<User> Users { get; set; }

        public Student FindItemByID(int id)
        {
            return (from student in this.Students
                    where student.StudentId == id
                    select student).SingleOrDefault();
        }
    }

}